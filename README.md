**HARDWARE PREREQUISITES**

- Operating System: Ubuntu 20.04 or higher
- RAM: 8GB or higher
- Free disk space: 40 GB
- High-speed internet connectivity


**Prerequisites**
- Install cURL
- Docker and Docker Compose
- Node.js and NPM

An automated script is provided to install these dependencies. To install these dependencies download and place the script file in a folder and execute the below commands. 

**Step1:**

`      $ chmod +x installDependencies.sh`
      

**Step2:**

`      $ ./installDependencies.sh`

     
**Step3:**

After completion of installation, **reboot** your system so that the changes are reflected to run docker as non-root user.

**Step4:**

`      $ ./installDependencies.sh bin`

**Install VScode**

Download VScode (https://code.visualstudio.com/)

`sudo dpkg -i <file_name>`

**Minifab commands**

`minifab netup -s couchdb -e true -o manufacturer.auto.com`

`minifab create -c autochannel`
 
`minifab join -c autochannel`

`minifab anchorupdate`

`minifab profilegen -c autochannel`

`minifab cleanup`

**Bring down the network**

`docker rm $(docker container ls -q) --force`

`docker container prune`

`docker system prune`

`docker volume prune`

`docker network prune`

